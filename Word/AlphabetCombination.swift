//
//  AlphabetCombination.swift
//  wordPractice
//
//  Created by stephenw on 2017/7/18.
//  Copyright © 2017年 stephenw.cc. All rights reserved.
//

import UIKit

class AlphabetCombinationViewController: UIViewController {
  
  let dictionary = ["2": "ABC", "3": "DEF", "4": "GHI", "5": "JKL", "6": "MNO", "7": "PQRS", "8": "TUV", "9": "WXYZ"]
  
  @IBOutlet weak var number1: CallerButton!
  @IBOutlet weak var number2: CallerButton!
  @IBOutlet weak var number3: CallerButton!
  @IBOutlet weak var number4: CallerButton!
  @IBOutlet weak var number5: CallerButton!
  @IBOutlet weak var number6: CallerButton!
  @IBOutlet weak var numer7: CallerButton!
  @IBOutlet weak var number8: CallerButton!
  @IBOutlet weak var number9: CallerButton!
  @IBOutlet weak var number0: CallerButton!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    number1.titleString = "1"
    number2.titleString = "2"
    number2.subTitle = dictionary["2"]!
    number3.titleString = "3"
    number3.subTitle = dictionary["3"]!
    number4.titleString = "4"
    number4.subTitle = dictionary["4"]!
    number5.titleString = "5"
    number5.subTitle = dictionary["5"]!
    number6.titleString = "6"
    number6.subTitle = dictionary["6"]!
    numer7.titleString = "7"
    numer7.subTitle = dictionary["7"]!
    number8.titleString = "8"
    number8.subTitle = dictionary["8"]!
    number9.titleString = "9"
    number9.subTitle = dictionary["9"]!
    number0.titleString = "0"
  }
  @IBOutlet weak var resultLabel: UILabel!
  
  @IBAction func callerButtonPressed(_ sender: CallerButton) {
    calculateStringCombienation(key: sender.titleString)
  }
  
  func calculateStringCombienation(key: String) {
    let origin = dictionary[key]!
    calculateCombinedString(string: origin)
  }
  
  func calculateCombinedString(string: String) -> Void {
    let characterArray = string.toArray()
    var result = ""
    performCombine(array: characterArray, offset: 0) { singleResult in
      result += " \(singleResult)"
    }
    self.resultLabel.text = result
  }
  
  func performCombine(array: [String], offset: Int, callback: (String) -> Void) -> Void {
    var mutableArray = array
    if offset == array.count - 1 {
      callback(array.reduce("", {"\($0)\($1)"}))
    } else {
      for i in offset..<array.count {
        mutableArray.swap(first: i, second: offset)
        performCombine(array: mutableArray, offset: offset + 1, callback: callback)
        mutableArray.swap(first: i, second: offset)
      }
    }
  }
}

extension String {
  func toArray() -> [String] {
    return self.characters.map({String($0)})
  }
}

extension Array {
  mutating func swap(first: Int, second: Int) {
    let temp = self[second]
    self[second] = self[first]
    self[first] = temp
  }
}

@IBDesignable
class CallerButton: UIButton {
  @IBInspectable var titleString = "" {
    didSet {
      self.setNeedsDisplay()
    }
  }
  
  @IBInspectable var subTitle = "" {
    didSet {
      self.setNeedsDisplay()
    }
  }
  
  override func draw(_ rect: CGRect) {
    let ctx = UIGraphicsGetCurrentContext()
    guard let _ = ctx else {
      return
    }
    let path = UIBezierPath(ovalIn: UIEdgeInsetsInsetRect(rect, UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)))
    ctx?.setStrokeColor(UIColor.black.cgColor)
    ctx?.addPath(path.cgPath)
    ctx?.strokePath()
    let centerRect = CGRect(x: rect.midX - 8, y: rect.midY - 15, width: 30, height: 30)
    if !self.titleString.isEmpty {
      (self.titleString as NSString).draw(in: centerRect, withAttributes: [NSForegroundColorAttributeName: UIColor.black, NSFontAttributeName: UIFont.systemFont(ofSize: 24)])
    }
    if !self.subTitle.isEmpty {
      (self.subTitle as NSString).draw(at: CGPoint(x:centerRect.minX, y:centerRect.maxY + 4), withAttributes:  [NSForegroundColorAttributeName: UIColor.black, NSFontAttributeName: UIFont.systemFont(ofSize: 9)])
    }
  }
}

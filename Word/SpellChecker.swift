//
//  SpellChecker.swift
//  wordPractice
//
//  Created by stephenw on 2017/7/19.
//  Copyright © 2017年 stephenw.cc. All rights reserved.
//

import Foundation

func edits(word: String) -> Set<String> {
  if word.isEmpty { return [] }
  
  let splits = word.characters.indices.map {
    (word[word.startIndex..<$0], word[$0..<word.endIndex])
  }
  
  let deletes = splits.map { $0.0 +  String($0.1.characters.dropFirst()) }
  
  let transposes: [String] = splits.map { left, right in
    if let fst = right.characters.first {
      let drop1 = String(right.characters.dropFirst())
      if let snd = drop1.characters.first {
        let drop2 = String(drop1.characters.dropFirst())
        return "\(left)\(snd)\(fst)\(drop2)"
      }
    }
    return ""
    }.filter { !$0.isEmpty }
  
  let alphabet = "abcdefghijklmnopqrstuvwxyz"
  
  let replaces = splits.flatMap { left, right in
    alphabet.characters.map { "\(left)\($0)\(String(right.characters.dropFirst()))" }
  }
  
  let inserts = splits.flatMap { left, right in
    alphabet.characters.map { "\(left)\($0)\(right)" }
  }
  
  return Set(deletes + transposes + replaces + inserts)
}


struct SpellChecker {
  
  var knownWords: [String:Int] = [:]
  
  mutating func train(word: String) {
    if let idx = knownWords[word] {
      knownWords[word] = idx + 1
    }
    else {
      knownWords[word] = 1
    }
  }
  
  init?(contentsOfFile file: String) {
    do {
      let text = try String(contentsOfFile: file, encoding: .utf8).lowercased()
      let words = text.unicodeScalars.split(whereSeparator: { !("a"..."z").contains($0) }).map { String($0) }
      for word in words { self.train(word: word) }
    }
    catch {
      return nil
    }
  }
  
  func knownEdits2(word: String) -> Set<String>? {
    var known_edits: Set<String> = []
    for edit in edits(word: word) {
      if let k = known(words: edits(word: edit)) {
        known_edits.formUnion(k)
      }
    }
    return known_edits.isEmpty ? nil : known_edits
  }
  
  func known<S: Sequence>(words: S) -> Set<String>? where S.Iterator.Element == String {
    let s = Set(words.filter { self.knownWords.index(forKey: $0) != nil })
    return s.isEmpty ? nil : s
  }
  
  func candidates(word: String) -> Set<String> {
    guard let result = known(words: [word]) ?? known(words: edits(word: word)) ?? knownEdits2(word: word) else {
      return Set<String>()
    }
    
    return result
  }
  
  func correct(word: String) -> String {
    return candidates(word: word).reduce(word) {
      (knownWords[$0] ?? 1) < (knownWords[$1] ?? 1) ? $1 : $0
    }
  }
  
  func inKnownWords(word: String) -> Bool {
    let result = knownWords[word]
    return result != nil
  }
}

class SpellCheckerManager {
  private init() {}
  var checker: SpellChecker? = nil
  public static let defaultManager = SpellCheckerManager()
  
  public func initializeChecker(callback: @escaping () -> Void) {
    if self.checker != nil {
      callback()
      return
    }
    let path = Bundle.main.path(forResource: "model", ofType: "txt")
    if path == nil {
      callback()
      return
    }
    DispatchQueue.global().async {
      self.checker = SpellChecker(contentsOfFile: path!)
      callback()
    }
  }
  
  public func stringIsWord(str: String) -> Bool {
    if self.checker == nil {
      return false
    }
    return self.checker!.inKnownWords(word: str)
  }
  
  public func stringWordCandidates(str: String) -> Set<String>? {
    if self.checker == nil {
      return nil
    }
    return checker!.candidates(word: str)
  }
  
}

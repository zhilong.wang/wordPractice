//
//  ViewController.swift
//  Word
//
//  Created by stephenw on 2017/7/18.
//  Copyright © 2017年 stephenw.cc. All rights reserved.
//

import UIKit

class HomeViewController: UITableViewController {
  let tableData = ["(1) alphabet to number",
                   "(2) number with alphabet combination",
                   "(3) select correct word"]
  let subViewControllerIdentifier = ["push_to_alphabetToNumber", "push_to_alphabet_combination", "push_to_correctWord"]
  let reuseIdentifier = "reuseable_cell"
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  // MARK: tableview dataSource and delegate
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return tableData.count
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier)
    let data = tableData[indexPath.row]
    cell?.textLabel?.text = data
    return cell!
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let segueIdentifier = subViewControllerIdentifier[indexPath.row]
    self.performSegue(withIdentifier: segueIdentifier, sender: nil)
  }
}


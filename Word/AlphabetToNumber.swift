//
//  AlphabetToNumber.swift
//  wordPractice
//
//  Created by stephenw on 2017/7/18.
//  Copyright © 2017年 stephenw.cc. All rights reserved.
//

import UIKit

class AlphabetToNumberViewController: UIViewController, UITextFieldDelegate {
  @IBOutlet weak var textField: UITextField!
  @IBOutlet weak var resultText: UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.textField.delegate = self
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    self.textField.becomeFirstResponder()
  }
  
  // MARK: textField delegate
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    let oldString = textField.text
    guard oldString != nil else {
      self.resultText.updateWithString(string: string)
      return true
    }
    if range.location == 0 && range.length == 0 {
      self.resultText.updateWithString(string: string)
      return true
    }
    if range.length == 0 {
      let newString = oldString! + string
      self.resultText.updateWithString(string: newString)
      return true
    }
    var newString = oldString as NSString?
    newString = newString?.replacingCharacters(in: range, with: string) as NSString?
    self.resultText.updateWithString(string: newString as String!)
    return true
  }
}

extension String {
  var asciiArray: [UInt32] {
    return unicodeScalars.filter{$0.isASCII}.map{$0.value}
  }
  func asciiString() -> String {
    var result = ""
    self.characters.forEach { character in
      let isAlpha = character.isAlpha
      if isAlpha {
        result += "\(character.asciiValue! - alphabetAValue)"
      } else {
        result += String(character)
      }
    }
    return result
  }
}
extension Character {
  var asciiValue: UInt32? {
    return String(self).unicodeScalars.filter{$0.isASCII}.first?.value
  }
  var isAlpha: Bool {
    return letters.contains(String(self).unicodeScalars.first!)
  }
}

let alphabetAValue = ("a".characters.first?.asciiValue)!
let letters = CharacterSet.letters
extension UILabel {
  func updateWithString(string: String) {
    self.text = string.lowercased().asciiString()
  }
}

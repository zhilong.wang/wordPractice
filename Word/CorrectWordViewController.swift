//
//  CorrectWordViewController.swift
//  wordPractice
//
//  Created by 王志龙 on 2017/7/19.
//  Copyright © 2017年 stephenw.cc. All rights reserved.
//

import UIKit

class CorrectWordViewController: UIViewController {
  
  let dictionary = ["2": "ABC", "3": "DEF", "4": "GHI", "5": "JKL", "6": "MNO", "7": "PQRS", "8": "TUV", "9": "WXYZ"]
  
  @IBOutlet weak var number1: CallerButton!
  @IBOutlet weak var number2: CallerButton!
  @IBOutlet weak var number3: CallerButton!
  @IBOutlet weak var number4: CallerButton!
  @IBOutlet weak var number5: CallerButton!
  @IBOutlet weak var number6: CallerButton!
  @IBOutlet weak var numer7: CallerButton!
  @IBOutlet weak var number8: CallerButton!
  @IBOutlet weak var number9: CallerButton!
  
  var currentStateString = ""
  var clickCount = 0
  var result = [String]()
  var currentClickKeys = ""
  
  @IBOutlet weak var loading: UIActivityIndicatorView!
  override func viewDidLoad() {
    super.viewDidLoad()
    number1.titleString = "1"
    number2.titleString = "2"
    number2.subTitle = dictionary["2"]!
    number3.titleString = "3"
    number3.subTitle = dictionary["3"]!
    number4.titleString = "4"
    number4.subTitle = dictionary["4"]!
    number5.titleString = "5"
    number5.subTitle = dictionary["5"]!
    number6.titleString = "6"
    number6.subTitle = dictionary["6"]!
    numer7.titleString = "7"
    numer7.subTitle = dictionary["7"]!
    number8.titleString = "8"
    number8.subTitle = dictionary["8"]!
    number9.titleString = "9"
    number9.subTitle = dictionary["9"]!
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated);
    self.loading.startAnimating()
    self.view.isUserInteractionEnabled = false
    SpellCheckerManager.defaultManager.initializeChecker {
      self.view.isUserInteractionEnabled = true
      self.loading.stopAnimating()
    }
  }
  
  @IBOutlet weak var resultLabel: UILabel!
  @IBOutlet weak var currentClickedKeysLabel: UILabel!
  
  @IBAction func callerButtonPressed(_ sender: CallerButton) {
    calculateStringCombienation(key: sender.titleString)
  }
  
  @IBAction func clearState(_ sender: Any) {
    clearAllScopes()
  }
  
  func clearAllScopes() {
    self.currentStateString = ""
    self.result = [String]()
    self.clickCount = 0
    self.resultLabel.text = "result will be displayed here"
    self.currentClickKeys = ""
    self.currentClickedKeysLabel.text = ""
  }
  
  func calculateStringCombienation(key: String) {
    if self.clickCount >= 5 {
      clearAllScopes()
      self.resultLabel.text = "click times over limit"
    }
    let origin = dictionary[key]!
    self.currentStateString += origin
    self.clickCount += 1
    self.currentClickKeys += key
    self.currentClickedKeysLabel.text = self.currentClickKeys
    calculateCombinedString(string: self.currentStateString)
  }
  
  func calculateCombinedString(string: String) -> Void {
    let characterArray = string.toArray()
    let words = calculateSubsets(source: characterArray)
    let actual = words.filter { (word) -> Bool in
      //limit word length to 20 for better performance
      if word.isEmpty || word.characters.count > 20 { return false }
      return SpellCheckerManager.defaultManager.stringIsWord(str: word)
    }
    if actual.count == 0 {
      self.resultLabel.text = "no results"
    } else {
      self.resultLabel.text = actual.reduce("", { "\($0!) \($1)" })
    }
    
//    let corrected = words.filter { (word) -> Bool in
//      let candidates = SpellCheckerManager.defaultManager.stringWordCandidates(str: word)
      // TODO: display candidates
//    }
  }
  
  func calculateSubsets(source: [String]) -> [String] {
    let result = subsetsWithDup(source)
    let words = result.map { (characters) -> String in
      return characters.reduce("", { $0 + $1 }).lowercased()
    }
    return words
  }
  
  func subsetsWithDup(_ n: [String]) -> [[String]] {
    var nums = n
    var result: [[String]] = [[]]
    nums.sort()
    var i = 0
    for _ in 0 ..< nums.count{
      var count: Int = 0
      while count + i < nums.count && nums[count+i] == nums[i] {
        count += 1
      }
      let prevSize: Int = result.count
      for k in 0 ..< prevSize {
        var tmp: [String] = result[k]
        for _ in 0 ..< count {
          tmp.append(nums[i])
          result.append(tmp)
        }
      }
      i += count
    }
    return result
  }
  
}

